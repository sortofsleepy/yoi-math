use std::f32::consts::PI;
use glam::{Vec3, Vec4};
use rand::{Rng};
use rand_distr::{Normal, Distribution};

/// unrolls a vec4 into an array.
pub fn pack_vec4(_vec: Vec<Vec4>) -> Vec<[f32; 4]> {
    let mut bytes = vec![];
    for v in _vec {
        bytes.push(v.to_array())
    }

    bytes
}

/// converts value to radians.
pub fn to_radians(deg: f32) -> f32 {
    let v = PI / 180.0;
    deg * v
}

/// converts values to degrees.
pub fn to_degrees(rad: f32) -> f32 {
    let v = 180.0 / PI;
    rad * v
}

/// Linear interpolates a value.
pub fn lerp(val: f32, min: f32, max: f32) -> f32 {
    min + (max - min) * val
}

/// helper to quickly init a vec 3
pub fn create_vec3() -> Vec3 {
    Vec3::new(0.0, 0.0, 0.0)
}

/// helper to quickly init a vec 4
pub fn create_vec4() -> Vec4 { Vec4::new(0.0, 0.0, 0.0, 1.0) }

/// Normalizes a value to something between 0-1
pub fn normalize_float(val: f32) -> f32 {
    1.0 / val
}

/// normalizes a RGB color value between 0-255 to 0-1
pub fn normalize_rgb(val: f32) -> f32 {
    val / 255.0
}

/// Normalizes a vec4
pub fn normalize_vec4(mut val: Vec4) -> Vec4 {
    val.x = normalize_float(val.x);
    val.y = normalize_float(val.y);
    val.z = normalize_float(val.z);
    val.w = normalize_float(val.w);
    val
}

/// Compares 2 Vec3 objects to see if they contain the same
/// values
pub fn compare_vec3(first: Vec3, second: Vec3) -> bool {
    let mut is_same = false;
    if first.x == second.x &&
        first.y == second.y &&
        first.z == second.z {
        is_same = true
    }

    is_same
}

/// Compares 2 Vec4 objects to see if they contain the same
/// values
pub fn compare_vec4(first: Vec4, second: Vec4) -> bool {
    let mut is_same = false;
    if first.x == second.x &&
        first.y == second.y &&
        first.z == second.z &&
        first.w == second.w {
        is_same = true
    }

    is_same
}

/// normalizes an rgba color assumed to be between 0 - 255
pub fn normalize_rgba_color(mut val: Vec4) -> Vec4 {
    val.x = normalize_rgb(val.x);
    val.y = normalize_rgb(val.y);
    val.z = normalize_rgb(val.z);
    val.w = normalize_rgb(val.w);
    val
}

/// normalizes an rgb color assumed to be between 0 - 255
pub fn normalize_rgb_color(mut val: Vec3) -> Vec3 {
    val.x = normalize_rgb(val.x);
    val.y = normalize_rgb(val.y);
    val.z = normalize_rgb(val.z);
    val
}

/// returns a random vec3
pub fn rand_vec3() -> Vec3 {
    Vec3::new(rand_float(0.0, 1.0), rand_float(0.0, 1.0), rand_float(0.0, 1.0))
}

/// Returns a Vec3 with values between the specified range.
pub fn rand_vec3_range(min: f32, max: f32) -> Vec3 {
    Vec3::new(
        rand_float(min, max),
        rand_float(min, max),
        rand_float(min, max),
    )
}

/// returns a random vec4
pub fn rand_vec4() -> Vec4 {
    Vec4::new(rand_float(-1.0, 1.0), rand_float(-1.0, 1.0), rand_float(-1.0, 1.0), 1.0)
}

/// Returns a Vec4 with values between the specified range.
pub fn rand_vec4_range(min: f32, max: f32) -> Vec4 {
    Vec4::new(
        rand_float(min, max),
        rand_float(min, max),
        rand_float(min, max),
        rand_float(min, max),
    )
}

/// returns a random f32 value between a min and max.
pub fn rand_float(min: f32, max: f32) -> f32 {
    let mut rng = rand::thread_rng();
    rng.gen_range(min..max)
}

/// Returns random i32 value between min and max.
pub fn rand_int(min: i32, max: i32) -> i32 {
    rand_float(min as f32, max as f32) as i32
}

/// returns a random value without any bounds.
pub fn rand_value() -> f32 {
    let mut rng = rand::thread_rng();
    let val: f32 = rng.gen();
    val
}

pub fn rand_gauss(min: f32, max: f32) -> f32 {
    let normal = Normal::new(min, max).unwrap();
    normal.sample(&mut rand::thread_rng())
}

/// Ensures a value lies between a min / max. If value is either less than min or grater than max,
/// return that particular value.
/// return that particular value.
pub fn clamp(x: f32, min: f32, max: f32) -> f32 {
    if x < min {
        return min;
    }

    if x > max {
        return max;
    }

    x
}